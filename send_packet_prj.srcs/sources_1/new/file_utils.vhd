library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

package file_operations is
 
 type ram_type is array (7 downto 0) of std_logic_vector (255 downto 0);

impure function get_packet (RamFileName : in string) return ram_type;
impure function get_tuser (RamFileName : in string) return std_logic_vector;

end package file_operations;

package body file_operations is

  impure function get_packet (RamFileName : in string) return ram_type is
        FILE ramfile : text is in RamFileName;
        variable RamFileLine : line;
        variable ram : ram_type;
            begin
                for i in 0 to 7 loop
                    readline(ramfile, RamFileLine);
                    hread(RamFileLine, ram(i));
                end loop;
                readline(ramfile, RamFileLine);
        return ram;
    end function;
 
  impure function get_tuser (RamFileName : in string) return std_logic_vector is
           FILE ramfile : text is in RamFileName;
           variable RamFileLine : line;
           variable tuser : std_logic_vector(255 downto 0);
               begin
                       readline(ramfile, RamFileLine);
                       readline(ramfile, RamFileLine);
                       readline(ramfile, RamFileLine);
                       readline(ramfile, RamFileLine);
                       readline(ramfile, RamFileLine);
                       readline(ramfile, RamFileLine);
                       readline(ramfile, RamFileLine);
                       readline(ramfile, RamFileLine);
                       readline(ramfile, RamFileLine);
                       hread(RamFileLine, tuser);
          
           return tuser(127 downto 0);
       end function; 
  end package body file_operations;