library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity send_packet_module is
  Generic (
  -- ETH header, 112 bits
  eth_dst_mac     : std_logic_vector(47 downto 0) := x"000c42363996";
  eth_src_mac     : std_logic_vector(47 downto 0) := x"0021591e7683";
  eth_pckt_type   : std_logic_vector(15 downto 0) := x"0800"; -- IPv4

  -- IPv4 header
  ip_ver          : std_logic_vector(3 downto 0)  := x"4";
  ip_hdr_len      : std_logic_vector(3 downto 0)  := x"5"; -- length of the header in nibbles
  ip_diff_serv    : std_logic_vector(7 downto 0)  := x"00"; -- differentiated services
  ip_tot_len      : std_logic_vector(15 downto 0) := x"003c"; -- packet total len in bytes
  ip_identif      : std_logic_vector(15 downto 0) := x"9f1b";
  ip_flags        : std_logic_vector(7 downto 0)  := x"40"; -- don't fragment
  ip_frag_offset  : std_logic_vector(7 downto 0)  := x"00";
  ip_ttl          : std_logic_vector(7 downto 0)  := x"34"; -- time to live
  ip_proto        : std_logic_vector(7 downto 0)  := x"06"; -- TCP protocol
  ip_checksum     : std_logic_vector(15 downto 0) := x"a368";
  ip_src          : std_logic_vector(31 downto 0) := x"4f062e26";
  ip_dst          : std_logic_vector(31 downto 0) := x"0a637ca9";

  --TCP Header
  tcp_src_port    : std_logic_vector(15 downto 0) := x"0231";
  tcp_dst_port    : std_logic_vector(15 downto 0) := x"c61c";
  -- void 32 bits for sequence number
  tcp_ack_num     : std_logic_vector(31 downto 0) := x"3984f97a";
  tcp_flags       : std_logic_vector(15 downto 0) := x"5018";
  tcp_win_size    : std_logic_vector(15 downto 0) := x"faf0";
  tcp_checksum    : std_logic_vector(15 downto 0) := x"7e83";
  tcp_urgent_ptr  : std_logic_vector(15 downto 0) := x"0000";
  -- 20 bytes of data

  packet_len      : std_logic_vector(15 downto 0) := x"004a" -- total packet len in bytes

);

Port (

       clock          : in std_logic;
       reset          : in std_logic;

       -- Control signals from OPP
       seq_num        : in std_logic_vector(31 downto 0);
       timestamp      : in std_logic_vector(31 downto 0);
       send           : in std_logic;
       done           : out std_logic;
       ready          : out std_logic;

       -- NF datapath AXI-S
       m_axis_tvalid  : out std_logic;
       m_axis_tdata   : out std_logic_vector(255 downto 0);
       m_axis_tready  : in std_logic;
       m_axis_tlast   : out std_logic;
       m_axis_tuser   : out std_logic_vector(127 downto 0)

     );

end send_packet_module;

architecture Behavioral of send_packet_module is

  type State_type is (reset_state, waiting, sending_1, sending_2, sending_3 );  -- Define the states
  signal state : State_Type; 
  signal m_axis_tuser_s : std_logic_vector(127 downto 0) := x"000000000000000000000000" & x"00" & x"00" & packet_len;
  signal seq_num_t   : std_logic_vector(31 downto 0) := (others => '0');
  signal frame_sent : std_logic_vector (4 downto 0) := (others => '0');
  
begin

 SEND_PCKT: process(clock)
  begin
    if rising_edge (clock) then
      if (reset = '1') then
  
      done <= '0';
      ready <= '0';
      frame_sent <= (others => '0');
      m_axis_tvalid <= '0';
      m_axis_tdata <= (others => '0');
      m_axis_tlast <= '0';
      m_axis_tuser <= (others => '0');
     
      state <= reset_state;
        
      else
        case state is

          when reset_state =>     

            state <= waiting;

          when waiting => 

            done <= '0';
            frame_sent <= (others => '0');
            ready <= '1';
            m_axis_tvalid <= '0';
            m_axis_tdata <= (others => '0');
            m_axis_tlast <= '0';
            m_axis_tuser <= (others => '0');

            if ((send = '1') and (m_axis_tready = '1')) then

              state <= sending_1;

            else 

              state <= waiting;

            end if;

          when sending_1 =>

            done <= '0';
            ready <= '0';
            m_axis_tvalid <= '1';
            m_axis_tdata <= eth_dst_mac & eth_src_mac & eth_pckt_type 
                            & ip_ver & ip_hdr_len & ip_diff_serv & ip_tot_len 
                            & ip_identif & ip_flags & ip_frag_offset & ip_ttl 
                            & ip_proto & ip_checksum & ip_src & ip_dst(31 downto 16);

            m_axis_tlast <= '0';
            m_axis_tuser <= m_axis_tuser_s;
            seq_num_t <= seq_num + 3;

            if (m_axis_tready = '1') then 
            
                state <= sending_2;
                
             end if;

          when sending_2 =>

            done <= '0';
            ready <= '0';
            m_axis_tvalid <= '1';
            m_axis_tdata <= ip_dst(15 downto 0) & tcp_src_port & tcp_dst_port & seq_num
                            & tcp_ack_num & tcp_flags & tcp_win_size & tcp_checksum & tcp_urgent_ptr
                            & (seq_num + 1) & (seq_num + 2) & seq_num_t(31 downto 16);

            m_axis_tlast <= '0';
            m_axis_tuser <= m_axis_tuser_s;

            if (m_axis_tready = '1') then 
                state <= sending_3;
            end if;

          when sending_3 =>

            done <= '1';
            ready <= '0';
            m_axis_tvalid <= '1';
            m_axis_tdata <=  seq_num_t(15 downto 0) & (seq_num + 4) & timestamp
                             & x"00000000000000000000000000000000000000000000";

            m_axis_tlast <= '1';
            m_axis_tuser <= m_axis_tuser_s;
            

            if (frame_sent < "11101") and (m_axis_tready = '1') then
                
                frame_sent <= frame_sent + 1;
                state <= sending_1;
            
            elsif (frame_sent >= "11101") and (m_axis_tready = '1') then
                
                state <= waiting;
            
            end if;

          when others =>
            state <= reset_state;

        end case;

      end if;
      end if;
    end process;
  
  end Behavioral;
