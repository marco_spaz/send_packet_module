library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity send_packet_tb is

  Port (

         done          : out std_logic;
         ready         : out std_logic;
         
         m_axis_tvalid : out std_logic;
         m_axis_tdata  : out std_logic_vector(255 downto 0);
         m_axis_tlast  : out std_logic;
         m_axis_tuser  : out std_logic_vector(127 downto 0)

);

end send_packet_tb;

architecture Behavioral of send_packet_tb is

  signal clock      : std_logic := '0';
  signal reset      : std_logic := '1';
  signal seq_num    : std_logic_vector(31 downto 0) := (others => '0');
  signal timestamp  : std_logic_vector(31 downto 0) := (others => '0');
  signal send       : std_logic := '0';
  signal tready     : std_logic := '0';

begin

  SEND_PACKET_MOD: entity work.send_packet_module port map (
                                                            clock,
                                                            reset,
                                                            seq_num,
                                                            timestamp,
                                                            send,
                                                            done,
                                                            ready,
                                                            m_axis_tvalid,
                                                            m_axis_tdata,
                                                            tready,
                                                            m_axis_tlast,
                                                            m_axis_tuser
                                                           );
clock <= not(clock) after 10ns;
reset <= '0' after 40ns;

seq_num     <= x"00000004";
timestamp   <= x"000000ff";
send <= '1' after 100ns, '0' after 140ns;
tready <= '1' after 120ns;

end Behavioral;
